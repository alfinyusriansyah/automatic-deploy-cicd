FROM python:3.8

# RUN pip install streamlit
# RUN pip install pandas
# RUN pip install tensorflow
# RUN pip install opencv-python
# RUN pip install matplotlib
# RUN pip install Pillow
# RUN pip install tensorflow-hub

EXPOSE 8501
WORKDIR /stm
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt
COPY . .

CMD ["streamlit","run","./stm.py"]